我好想吃冰激凌啊。

夜晚的冷空氣使得我身體一抖，我將外套的衣領往胸口拉，逃避著現實。

不對，正確地來講，我身體會發抖的原因並非是空氣太冷。而是不知為何從獵人轉職為獵手的莉茲。

緹諾拚命地在石柱上面來回跳躍，尋找某種不存在的事物。
而吩咐她這樣做的莉茲，則是在我眼前一個勁地堆積起沙兔的頭部。

莉茲比起這附近生存的任何野獸都還要敏捷，而且她不是為了填飽肚子而狩獵，所以到現在都還沒停手。

我所注意到的沙兔就只有一只，不過真不愧是支撐起生態系的種族，這附近似乎有許多沙兔的樣子。
莉茲將它們抓到我面前，舉起來示意以後，像是理所當然地把頭部砍下，逐漸在我眼前堆成一座屍頭山。
黑暗之中，死後仍是發亮的眼睛實在是令人感到毛骨悚然。儘管到中途我就不敢再看下去，不過沒辦法徹底隔絕那些聲音。不幸中的萬幸是，兔子都沒有發出大聲的鳴叫。

你是要吃這些嗎？不會吃的對吧？快住手吧，這是在虐待動物。不對，姑且是分類為魔物，不過那感覺就像是動物一樣不是嗎。

我是博愛主義者，當然會討厭受到暴力的摧殘，可也不怎麼喜歡對別人施以暴力。殺害魔物並不是犯罪，所以這成不了阻止莉茲的理由，不過還是太殘忍了。

緹諾不都說過請看著她。你就看看她嘛，畢竟我什麼都看不見。
我深深地嘆了口氣，決定勸告她。

自從成為獵人以後，我總是受到別人的幫助。我欠了莉茲她們不少的人情。
可這件事跟現在是兩碼事。

「莉茲⋯⋯做到這種程度也該停手了吧？」
「誒？真的？」

聲音停止，不知是狩獵多少只，即便背過屍體也能聞到強烈的血味。
即便我已經習慣一定程度的氣味，可這氣味都要讓我嘔吐出來了。

「⋯⋯我是⋯⋯博愛主義者」

我不會看錯她的。我和莉茲之間的牽絆還沒弱到因為這種事情就對她失望。
然而我語氣還是難免會變得很冷淡。聞言，莉茲繞到我面前，歪著頭。

她一副純屬感到疑惑的眼神。

「？意思是不要再獵殺小嘍囉，而是獵殺本體？」

？？？究竟在說什麼鬼，我怎麼聽不懂。又是獵人術語嗎？

「不過，雖說是小嘍囉，可小嘍囉甚至都不知道自己遭到利用，即便是我也不知道主謀究竟在哪裡，似乎相當戒備我們。像這種類型的事情，還是露西亞或者絲特莉比較清楚吧」

莉茲以指尖抵住雙唇，快口說著。我拚命理解她話裡的意思，可還是完全聽不懂她在說什麼。

總之，完全聽不懂的前半句先丟到一邊，來思考後半句。

戒備我們？那是當然的好吧，這麼多同伴被殺害。沙兔是會群居的生物，戒備別人肯定是會的。雖說就現在的狀況來看，怎麼戒備也是無濟於事就是了。

然後，露西亞和絲特莉很了解兔子嗎⋯⋯⋯這我還是第一次知道，魔導師和錬金術師都具備一些奇妙的知識，所以應該就是這樣吧。
她們三人要是認真起來，別說是兔子，根本就不會留下半點東西。明明不用這麼認真狩獵兔子也行的。

嗯，我感覺可以理解她話裡八成左右的意思了。重點就是阻止莉茲繼續虐殺。
我望向什麼都看不見的黑暗，裝模作樣地說道。

「總之，沒必要再繼續獵殺兔子了啦。說到底，被戒備其實也無所謂的」
「真不愧是克萊伊，好帥！可是主謀還沒有擊潰，人家覺得要讓對方後悔對我們出手才行。明明都跟我們敵對了，還以為躲起來就可以活下去，這樣不是有損我們的面子？　賭上『嘆靈』之名，絕對要擊垮他們⋯⋯以為我們是連居住地都找不到的廢物嗎？人家覺得繼續被瞧不起可不好。一定要把對方擊垮到再也不敢與我們敵對才行。如果是露西亞和絲特莉也絕對會這麼說哦？」

莉茲眼神熠熠生輝。我所要說的意思似乎沒有傳達給她。

面對兔子，你會不會太過於全力以赴了啊。
對方可是沙兔耶？如果是人類倒姑且不說，可那是沙兔喔？
被沙兔瞧不起其實也不會有損我們面子。歸根究底，它們沒給任何人添麻煩，只是在這個弱肉強食的世界裡謹慎過活罷了。又沒有襲擊我們。
總會全力以赴是莉茲的優點，不過我求你考慮到對象再說。

而且，你把露西亞和絲特莉想成什麼樣的人了。
她們不會說的，畢竟她們討厭無謂的事情。如果莉茲硬是要求，也許是會奉陪。不過只要不是這樣，很難想像她們會去殲滅無害的兔子。

認真地思考莉茲所說以後，我覺得實在是過於愚蠢，露出笑容來。

在這時，我才意識到自己錯了。

莉茲雙手在背後交叉，以一副期待不已的目光仰望著我。

沒錯。莉茲雖然有點欠缺考慮，可也不是笨蛋。儘管狂暴，然而有幽默感。對我來說就是重要的朋友。

既然如此，莉茲所說的──估計是玩笑話吧。最近我們交流太少了，所以她說不定是想借用這種方式來彌補。
雖然為了單純的玩笑就搞出一座沙兔的屍體山，令人有些難以接受。可這件事也不值得特意去找她的過錯。

今天的我頭腦靈光。

（颯：出現第三次）

「不，說到底兔子什麼的根本就不用放在心上⋯⋯而且你說沒辦法找到居住地？主謀？王？兔子之王肯定就在兔子的巢穴裡吧，想都不用想」

也許是會出來外面，不過基本上都呆在巢穴裡面吧。說到底，沙兔的群體中是否有王都是一個疑問⋯⋯⋯

聽到我自信滿滿的回應，莉茲的表情就像是花朵綻放一般十分明亮。

「莉茲總是用眼睛來尋找，所以才找不到啦。要好好動動腦子才行」
「誒～我明明也動腦子了。⋯⋯是嗎，原來在巢穴裡面啊⋯⋯畢竟這附近有許多符合的巢穴，也許是有適合那位膽小的飼主的隱藏地點⋯⋯」

飼主是什麼啊，沙兔哪有什麼飼主。

「嗯嗯。說的對。這種事情怎麼樣都無所謂啦，你就看著自己弟子的英姿吧」

雖然我眼睛什麼都看不見，不過她應該正在努力吧。
莉茲不知為何揚起嘴角，露出冷酷的笑容。我抓住她肩膀，讓她面向圓柱那邊。雖然不知道她是不是認真的，不過還是別再執著兔子了。

微弱的月光照耀下，漆黑的人影做著些什麼事。我只能知道這些。
在幾米高的柱子之間來回跳動的景象，感覺就像是見鬼似的。

「她已經夠出色的了⋯⋯不都是獨當一面的盜賊麼」
「誒～真的？鍛鍊還不夠啦。得快點讓她成長，當人家對練的對手才行⋯⋯」

雖然我覺得鍛鍊已經夠充分了，不過莉茲還是不滿足的樣子。嘛，她會收弟子的原因之一，似乎就是為了將緹諾訓練得比起自己還強，作為練習的對象，所以也是沒辦法。
話說我以前就隱約在想了，訓練得比起自己還強不是很矛盾嗎？是要怎麼做啊。

我抱著很想吃冰激凌的心情安撫著莉茲。在緹諾結束調查之前，我必須要讓她接受來到這座寶物殿的理由才行。

「我呢，是想看看緹諾的成長才來這座寶物殿的。在『白狼之巣』那時她也許是需要你幫忙了，可只是運氣不好啦」
「誒～小緹就算了，也看看人家的成長嘛」

莉茲在被我抓著肩膀的姿勢下，突然抬起頭，仰望著我。

我看了也不懂。雖然我是覺得你足夠強了啊。
莉茲他們已經遠遠超過我了。就連緹諾也不會輸給我。只是看看還行，我可給不出什麼建議⋯⋯

我決定搪塞過去。
我用手掌撫摸莉茲那微微泛黑，顯得十分健康的脖子。而她則是眼神濕潤，放鬆肩膀，身體顫抖。
體溫似乎上升許多的樣子。

莉茲所積累的動力是受到高性能調節器所控制。

（颯：莫名其妙的比喻）

而所謂的能量，簡單地來講就是熱量。她在發揮實力時，體溫會高到不同尋常。
她不同於以卓越的技術與鬥志來作為武器的路克，以及利用身體頑強性的安瑟穆。

「一樣的一樣的，只要看見緹諾的成長就能知道你有多麼的努力。莉茲你就是太发憤圖強了。再稍微放鬆一些吧」
「也就是要人家以自然的樣子應對一切事態？嗯～本來我們還以為自己已經度過許多修羅場了，不過還遠遠達不到那種領域嗎。自從克萊伊去照顧那些廢物以後，探索也變得無聊起來了」
「嗯嗯，說的也是呢？」

⋯⋯雖然不知道她在說什麼，不過既然很高興那就算了。

莉茲點點頭，似乎要讓自己接受事實一樣，隨後將身體靠向我這邊。

「嗯，不過我有點打起精神了，謝謝克萊伊。我會努力的，不會再讓克萊伊失望」
「⋯⋯我也沒有失望啊」

不如說我反而在擔心莉茲她們會不會對我感到失望透頂。
這時，從圓柱上傳來緹諾蘊含安心與（難得會）興奮的聲音。

「Master，寶具！有寶具！是手環型的！我找到了，Master！Master！姐姐大人！」
「哦，挺能幹的嘛」

在這麼低級的寶物殿是很少會找到寶具的。也許是因為幾乎沒有人會來這裡。
所謂的善有善報吧，跟我是完全不一樣。

畢竟都得到肉眼可見的成果，莉茲也應該滿意了吧。
我這麼想著，低頭一看。莉茲的表情上已經沒有剛才那高興的樣子，她蹙起眉頭咂嘴一聲。

「蠢貨，這小緹。居然這麼大意。克萊伊怎麼可能會為了一個寶具就來到這種地方。就沒感覺到這股震動嗎」

⋯⋯嗯？

一股仿彿地面都要翻過來的震動襲向全身。
就連緹諾也嚇一跳了吧，她發出短促的慘叫，跳了起來。

唯有莉茲一直是保持著冷靜。

這不是地震，搖動也沒有停止的跡象。
大腦一片混亂，我什麼話都說不出來。甚至都無法發出慘叫，全身動彈不得。

莉茲像是要讓我冷靜下來，緊抱住我的手。於是我才稍微冷靜一些。

緊接著，我望見超出預料的光景而啞口無言。

聲音與震動的來源就在於圓柱群的正中央。在圓柱包圍下的遼闊空間裡，升起一塊巨大的黑團。
它的高度有四米左右，等同於周圍的圓柱。乍看之下就像是同樣的圓柱，可在遠方凝神細看以後，就能發現那根柱子有著『四肢』

黑暗之中，只見那頭部上閃耀著猶如倒三角形的奇妙花紋。

那是什麼？

莉茲以食指抵住嘴唇，一副嚴肅的表情喃喃自語。

「格雷姆。金屬裝甲。腳部有助推器，雙手各有一門炮。裝備有盾牌和長劍。沒有翅膀。高科技陸戰型格雷姆？紋章是絲特莉說過的『虛空』。⋯⋯⋯⋯克萊伊的目的就是這個嗎⋯⋯嗯，感覺的確是很強」

格雷姆？啊，對了。姑且是這裡會出現的幻影。
我聽著莉茲分析戰況，並裝作一副很懂的樣子。

「嗯嗯，說的是。那是巨大的沙石格雷姆呢」

『白狼之巣』也是這樣，最近寶物殿會不會太可怕了。相較於寶物殿的難度，這些疑似BOSS的傢伙級別會不會太高了？

體格龐大就等同於強大。龍只要利用它的體格就可以屠殺無數的獵人。
嘛，雖說眼前的沙石格雷姆並沒有大到那種地步，不過跟緹諾相比，那就實在是大得多了。

巨大的沙石格雷姆轉過身望向這邊。儘管我看不清楚，不過從輪廓來看，那具巨大的人形就正如莉茲所說的，雙手持拿著大劍與盾牌。
要是我還從容不迫，現在早就立刻逃跑了吧。可腳動不了。

莉茲難得露出一副險峻的神色。

「嗯～再怎麼樣，總覺得光憑我和小緹是沒辦法應付的呢。畢竟我們不怎麼擅長戰鬥。克萊伊嘴上說很簡單，可還是挺苛刻的呢」
「⋯⋯是緹諾選的這裡」

啊啊，我感覺要吐了。