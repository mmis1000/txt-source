在緹諾居住的精緻小屋裡，不堪入耳的罵聲正交錯響起。

房子的主人緹諾毫無辦法地，在角落佝僂著身子並注視著那個場面。
基本上，獵人越是攻略寶物殿，物理上就會變得越強。因為能提升基礎能力，甚至還可以積累經驗，所以要戰勝等級更高的獵人是非常困難的。
作為獵人，緹諾自負已經達到中堅水平，但與兩位姐姐大人無疑是天差地別。

「不要擅自誘惑我家小緹！小緹可是克萊伊醬托付給我的！克萊伊醬說過可以隨便使喚，然後托付給我的！」

師父莉茲青筋暴起，尖聲喊道。
從她身上升起的能量與上位獵人相稱地龐大。

「那是因為姐姐撒嬌了吧！明明是我最先預定接收的──如果當初托付給我，可能小緹現在早就成為能在天上飛，眼睛可以射出光線的，史無前例的超級獵人了！」

與之相反，另一位姐姐大人則以略低的聲音答道。
雖然和前者相比保持著冷靜，但從她身上涌出的力量也絲毫不遜於師父的力量。

如果擁有同等才能的人編在同一個隊伍，並且擁有相同的經歷，瑪那源的吸收量就會達到同等程度。儘管職業大為不同，可在緹諾看來兩人都一樣。

兩位姐姐大人既是值得尊敬的對象，也是令人畏懼的對象。
平時冒險獨自潛入寶物殿的緹諾，一旦面對這兩人就只能瑟瑟發抖地等待事態平息。

「況且，比起採用奇怪的鍛鍊方法的師父，小緹更喜歡既有錢又能輕易給她力量的師父吧？」

「啊～！？他人給的力量沒有意義吧！說到底，西特不僅會添加還會刪減吧！」

「我會看對象來的！小緹的話就沒有剝奪自由意志的必要，因為嬌小也方便帶在身邊，最合適對吧！？」

「喂，小鬼們！不要突然吵架啊，小姑娘在害怕不是嗎！」

剛才還綳著臉旁觀的馬蒂斯，介入到互相表露戰意的兩人之間。
然而，兩人的氣勢卻沒有減弱的跡象。

「那個寶具是，我和小緹發現的東西！別多管閑事！」

「不是姐姐，而是小緹發現的東西！小緹說要讓給我，所以我應該有權利吧！？」

我沒有說過⋯⋯西特莉姐姐大人。

雖然打算反駁，卻恰好被視線盯上，結果什麼都說不出來。
手鐲型寶具『舞動光影』的所有權完全脫離了緹諾的意願。
儘管短劍和禮服很令人高興，可對緹諾來說，如果能讓Master高興，自己想親自送去。但事到如今，即使要求還回來也是徒勞吧。

「再說，別總是在關鍵時刻來攪局啊！你這只偷腥貓！給我在研究室悶著！」

「要怪就怪姐姐總是給人添麻煩吧！你知道我為了掩蓋有多麼辛苦麼──」

「哈～！？盧克醬也是一樣啊，再說我又沒有拜託你掩蓋！」

「盧克桑的情況是，對方處於不能說的狀態才沒有問題！所以只有姐姐！」

莉茲終於開始把落在地板上的寶箱扔向西特莉。
對於毫不留情地被扔過來的寶箱，西特莉發揮出錬金術師不應該有的優秀反射神經，用放在桌上的托盤代替盾牌防住。
被彈開的寶箱破壞掉櫥櫃，扎進牆壁裡。窗戶玻璃破碎，驚人的聲音響起。

緹諾鼓起最後的勇氣，發出微弱的聲音。

「請住手，姐姐大人！會被鄰居訓斥的──可是我！」

緹諾敏捷地用雙手接下順勢而來的茶杯和茶壺。在飛來飛去的家具中，瞬間判斷出易碎品，然後接住並放到角落。真的是拼了命。
現在不是吃飯時間真是太好了。茶杯和茶壺姑且不論，如果刀叉飛過來，很有可能會受傷。

至少，為了保護馬蒂斯，要把飛過來的東西擊落。
馬蒂斯因眨眼之間就變得激烈的姐妹吵架而戰慄著。即使東西飛來飛去，兩人也沒有停止互相咒罵。

「誰，誰來轉交都一樣吧⋯⋯」

「折，折衷處理⋯⋯我來轉交如何──」

兩人已經聽不進緹諾的話了。
只是在扔那邊的東西也還好，但若是放任不管，就會開始扔起匕首和藥水，而緹諾的家將會毀掉一半。然後，在用光微薄的積蓄修好房子之前，緹諾都要生活在破爛不堪的家裡。

居中調解實在是做不到。無論是師父還是西特莉姐姐大人，都不會因緹諾一個人介入而停手。
能勸架的只有同隊伍的成員，或是伊娃等與Master關係密切且具有常識性認知的人。然而，那樣的人很少來緹諾家。

雖然很混亂，但還是拚命地擊落流彈，並考慮著怎麼做才能最大程度地減少損失，就在此時，緹諾忽然聽到玄關的門被敲擊的聲音。

§ § §

「Master！我好想見你～～～！」

這是怎麼回事啊。我單手拿著蛋糕盒，睜圓雙眼。
俯視著在看到我的瞬間，異常興奮地叫起來並向我抱過來的緹諾。簡直是一頭霧水。

我的確是想讓她看看有點冷酷又很體貼的Master，但沒想到會受到如此熱烈的歡迎。

好久沒看過的緹諾家真是慘不忍睹。
地板上雜亂地散落著蓋子打開的寶箱，破碎的窗戶玻璃以及櫥櫃。就像是有些漂亮的廢墟一樣。
是在忙什麼嗎？

在客廳的中央，正站著眼熟的斯馬特姐妹。
她們看向這邊，並掛著難以言喻的笑容。

莉茲揮起不知為何握著匕首的右手，西特莉則把鮮紅似火的藥水瓶藏到背後，並鼓起臉頰。

「啊⋯⋯⋯⋯克萊伊醬，早上好」

「看吧，都是因為姐姐說任性話，所以克萊伊桑來了──。克萊伊桑，早安」

「小，小子，慢死了！趕緊來啊！」

西特莉和莉茲在緹諾家並不奇怪，可為什麼今天馬蒂斯也在。
而且不知為何漲紅著臉，並瞪著我。明明只是來送蛋糕，為什麼會陷入這種狀態⋯⋯⋯

我把手放到近在咫尺的緹諾的頭上，一邊梳理著頭髮一邊歪起頭。
雖然完全不清楚狀況──。

「總之⋯⋯先給我正坐」

§

「不是的，克萊伊桑。這裡面存在誤會⋯⋯」

「那個呢，簡單地說，就是西特和小緹要搶我的功勞。那個，我沒有錯對吧？」

西特莉和莉茲兩人並排端坐在地毯上，並向我辯解道。
我坐到椅子上，深深地嘆了一口氣。身旁總算冷靜下來的緹諾，不知為何正用尊敬的眼神看著我。

「不像樣。真是不像樣」

雖然不太清楚狀況，但謝罪可不是那麼做的。
在身為謝罪大師的我看來，根本是不及格。

西特莉和莉茲聽到我的批評，含淚陷入沉默。這樣並列來看，就能清楚地明白兩人是姐妹。

從以前開始西特莉和莉茲就經常吵架。從爭吵到扭打，對我來說是司空見慣的場面，但由於在得到力量後，也是以同樣的感覺開始吵架，因此對周圍的人來說，可能受不住吧。

「為，為什麼克萊伊桑⋯⋯⋯⋯那個，我們並不是真的在吵架⋯⋯」

「沒錯沒錯。因為這種程度，就像是準備運動一樣！小緹已經習慣了，用不著克萊伊醬出面也⋯⋯⋯⋯」

兩人做出像是感到內疚一樣的謝罪舉動。和我不同，她們應該是極少有謝罪的機會，看起來非常難做到。
我只是來送蛋糕而已啊。

對於一反常態變得安分的兩人，馬蒂斯無語似地說道。

「⋯⋯還是老樣子，弱點是小子呢⋯⋯」

「可不是白白相處那麼久的」

「不，這才不是那種問題──」

反正這次也是莉茲先動手的吧。畢竟她會因無心之事而動手呢⋯⋯⋯

但是，她也決不是愛鬧的人。只要好言相勸就會變得安分。只是不聽勸的事⋯⋯太多了。

「不愧是你，Master⋯⋯真的，真的非常感謝。能制止姐姐大人們吵架的只有Master而已」

緹諾的眼裡噙著淚水，以與其說是尊敬，不如說更接近崇拜的眼神說道。
抱歉。真的很抱歉。

「小緹⋯⋯之後給我記著」

「我明明只是為雙方的利益著想⋯⋯」

莉茲緊緊握住拳頭並瞪著緹諾，西特莉則像乞求憐憫一樣，視線朝上看著我。看起來似乎沒有反省的意思。
我也並不是說禁止姐妹吵架。畢竟都說關係好才會吵架呢⋯⋯⋯

可是，明明不知道吵架的原因卻在責備她們，真是有些滑稽。
當我正完全覺得事不關己時，可能是從我的沉默中感受到了什麼，西特莉巧妙地保持著正坐，並一點一點地蹭過來。一來到我的腳邊，就抱緊我的腿，接著發出高漲的聲音。

就算與莉茲和緹諾相比，西特莉的身材也算好的。具體來說，就是胸部很大。
被那樣抱住後，便感受到柔軟的觸感，心裡非常地慌亂。

「對不起，克萊伊桑。我沒有要添麻煩的打算。如果時間再多一些，我應該就能和平地做出了斷！」

我可禁不住被胸部貼住啊⋯⋯不，不會有禁得住那個的男人吧。
不過看到那副景象，時間再多一些也無法和平地了結吧。是打算使用什麼樣的魔法呢。

「克萊伊醬，我也是，給克萊伊醬添麻煩的打算，可沒有哦？明明小緹和西特退讓一步就可以了⋯⋯」

像是要與那個競爭一樣，莉茲站立起來，猛地纏住我的膝蓋。感覺上就像王一樣。
我什麼都沒去想，煞有介事地點點頭，並打了一個響指。

「嗯嗯，是呢⋯⋯總之你們兩個，先去打掃吧」

西特莉和莉茲雙雙站立起來。這是即使不清楚狀況也能解決問題的證明。

「！最喜歡打掃了！我會努力的！」

「西特，你去籌備玻璃。我來收拾⋯⋯⋯⋯⋯如果以後不用更結實一些的玻璃，可就麻煩啦」

「姐姐大人，已經是最結實的玻璃了──不，沒事，什麼都沒有」

西特莉跑了出去，莉茲則開始收拾起掉在地板上的餐具和倒下的架子。由於這不是第一次，所以很熟練。
再過一段時間房間就會恢復原狀吧。
我今天重新認識到的只有，莉茲和西特莉的謝罪完全不像樣這點⋯⋯⋯還有胸部貼上來的感覺非常不錯。

對於望著遠處的我，稍微恢復活力的緹諾戰戰兢兢地問道。

「說起來，Master。今天因何事而來？」

「啊啊。我帶來了新作的蛋糕喲。打算作為謝禮」

畢竟莉茲和西特莉總是給你添麻煩⋯⋯⋯

正要接著那麼說時，我注意到緹諾似乎因為感動而流著眼淚。

「嗚嗚⋯⋯非⋯⋯非常感謝，Master。我會一生追隨你」

「唔，嗯。嘛～。用不著那麼誇張⋯⋯好了，別哭了⋯⋯」

⋯⋯今天的緹諾反應太誇張啦。

不過是帶來一個蛋糕而已⋯⋯難道平時應該再體貼一些嗎。

緹諾吸著鼻子，擦拭起濕潤的眼睛並打開蛋糕盒。看到裡面裝著的兩塊蛋糕，眼睛瞬間睜圓，但又恍然大悟似地點了點頭。

「不愧是你，Master。另一塊是馬蒂斯的吧？」

？？？？？？？

那可是我的？另一塊可是我的？而且我才不知道馬蒂斯在這裡。

「哼。多餘的用心⋯⋯既然有考慮那種事的閑工夫，就趕緊來幫助小姑娘啊」

才沒有用心喲。我沒有給的打算，不要的話不是挺好嗎？
當我正因為出乎意料的話而愣住時，能幹的後輩緹諾立即幫腔道。

「Master是重情重義的人。別說那種話──Master的蛋糕可是絕品」

「⋯⋯嘖。被說成那樣就不能不收下了⋯⋯⋯我帶回家給孫女吧」

「⋯⋯嗯嗯，是呢」

我到底是來做什麼的啊？